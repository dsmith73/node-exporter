Role Name
=========

Role to install node-exporter

Requirements
------------

None

Role Variables
--------------

Simple default variables for node-exporter download location and location to store executable.

Dependencies
------------

None, but will be useful with Prometheus role.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - node-exporter

License
-------

BSD

Author Information
------------------

John Hooks
